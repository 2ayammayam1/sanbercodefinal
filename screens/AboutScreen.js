import React, {Component} from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, Linking } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const socials = [
  {
    name: 'Instagram',
    icon: 'instagram',
    uri: 'https://www.instagram.com/mariefismirianda/'
  },
  {
    name: 'Facebook',
    icon: 'facebook',
    uri: 'https://facebook.com/ismirianda'
  },
  {
    name: 'Github',
    icon: 'github',
    uri: 'http://github.com/mariefismi02'
  }
]

const URLOpen = (uri) => Linking.openURL(uri).catch((err) => console.error('An error occurred', err))

//import { Header } from "@react-navigation/stack";
export default class AboutScreen extends Component {
    render(){
    return (
      <View style={styles.container}>
        <View style={{alignItems: "center"}}>
          <Image 
            source={require('../images/profile.jpg')}
            style={styles.circularImage}
          />
          <Text style={styles.nameText}>Muhammad Arief Ismirianda</Text>
        </View>
        <View style={styles.horizontalLine} />
        <Text style={styles.roleText}>Game Developer</Text>
        <View>
          <View style={styles.container2}>
            <TouchableOpacity onPress={() => URLOpen('mailto:marief.ismirianda@gmail.com')}>
              <Text style={styles.subtitleText}>Email</Text>
              <Text style={styles.itemText}>marief.ismirianda@gmail.com</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.container2}>
            <TouchableOpacity onPress={() => URLOpen('https://mariefismi02.github.io/portofolio/')}>
              <Text style={styles.subtitleText}>Portofolio</Text>
              <Text style={styles.itemText}>mariefismi02.github.io/portofolio</Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            { 
                socials.map( (item, index) => {
                    return (
                        <TouchableOpacity style={styles.container3} key={index} 
                          onPress={() => URLOpen(item.uri)}>
                            <Text style={[styles.subtitleText, {marginLeft: 0}]}>{item.name}</Text>
                            <FontAwesome style={{margin: 8}} name={item.icon} size={48} color="#003366" />
                        </TouchableOpacity>
                    )
                }

                )
            }
          </View>
          
        </View>
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  bar: {
    backgroundColor: '#B4E9FF',
    padding: 10,
    flexDirection: 'row'
  },
  titleText: {
    color: '#003366',
    fontSize: 24,
    fontWeight: "bold"
  },
  circularImage: {
    width: 200 , 
    height: 200, 
    borderRadius: 200/ 2,
    borderWidth: 5,
    overflow: 'hidden',
    marginVertical: 20
  }, 
  nameText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 24,
    fontWeight: "bold",
    marginTop: 10
  },
  roleText: {
    textAlign: 'center',
    color: '#3EC6FF',
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 5
  },
  container2: {
    backgroundColor: '#B4E9FF',
    marginHorizontal: 8,
    marginTop: 8,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    },
  subtitleText: {
    color: '#003366',
    fontSize: 12,
    marginLeft: 8,
    marginTop: 5,
    fontWeight: 'bold'
  },
  horizontalLine: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical: 8
  },
  itemText: {
    color: '#003366',
    fontSize: 16,
    marginVertical: 8,
    marginLeft: 8
  },
  container3: {
    backgroundColor: '#B4E9FF',
    alignItems: "center",
    justifyContent: 'center',
    margin: 8,
    paddingVertical: 8,
    paddingHorizontal: 24,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  constactList: {

  }
  
});
