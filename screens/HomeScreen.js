import React, {Component} from 'react';
import { StyleSheet, FlatList, Text, View, TextInput, TouchableOpacity, ActivityIndicator, Image } from 'react-native';
import axios from 'axios';



export default class HomeScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      news: [],
      isLoading: true,
      isError: false,
      searchText: ''
    }
  }

  componentDidMount() {
    axios.get(`http://newsapi.org/v2/top-headlines?country=id&apiKey=5777b95ba4c64dd3be93f327ffd31bf3`)
      .then(res => {
        const news = res.data.articles;
        this.setState({ news, isLoading: false });
      }).catch(e =>
        this.setState({isError: true, isLoading: false})
      )
  }

  static showDetail(navigation){
    navigation.navigate("Detail")
  }

    render() {
    return (
      <View style={styles.container}>
        <TextInput style={styles.input} placeholder="Cari disini"/>
        {this.state.isLoading && <ActivityIndicator size='large' color='red' />}
        {this.state.isError && <Text style={{textAlign: 'center', color: 'red'}}>Something went wrong!</Text>}
        <View style={styles.headlines}>
          <Text style={styles.headlinesText}>Headlines</Text>
        </View>
        
        <FlatList
            data={this.state.news}
            renderItem={(newsItem)=><NewsItem newsItem={newsItem.item} navigation={this.props.navigation} />}
            keyExtractor={(item, i)=>i.toString()}
            ItemSeparatorComponent={()=><View style={{height:0.5}}/>}

          />
      </View>
    );
    }
}

class NewsItem extends Component {
  render() {
    let newsItem = this.props.newsItem;
    let navigation = this.props.navigation;

    return (
      <TouchableOpacity onPress={() => HomeScreen.showDetail(navigation)}>
        <View style={styles.itemContainer}>
        <View style={{flex: 1}}>
          <Text style={styles.subtitleText}>{newsItem.publishedAt}</Text>
          <Text maxLength = {8} style={styles.titleText}>{ ((newsItem.title).length > 68) ? 
    (((newsItem.title).substring(0,68-3)) + '...') : 
    newsItem.title }</Text>
        </View>
        <View style={{justifyContent: 'space-around'}}>
          <Image
            source={{ uri: newsItem.urlToImage }}
            style={{ width: 77, height: 68 }}
          />
          <Text style={[styles.subtitleText, {textAlign: 'right'}]}>{newsItem.source.name}</Text>
        </View>
      </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  titleText: {
    fontWeight: 'bold',
    color: '#003366',
    fontSize: 18,
    fontFamily: 'Roboto',
  },
  subtitleText: {
    fontWeight: 'bold',
    color: '#3F6286',
    fontSize: 11,
    fontFamily: 'Roboto'
  },
  input: {
    borderRadius: 10,
    backgroundColor: '#C4C4C4',
    borderColor: '#003366',
    height: 64,
    margin: 8,
    fontSize: 16,
    paddingLeft: 8
  },
  inputTitle: {
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10
  }, 
  buttonText: {
    color: '#FFFFFF',
    fontSize: 24
  }, 
  itemContainer: {
    backgroundColor: '#B4E9FF',
    margin: 8,
    borderRadius: 10,
    padding: 16,
    flexDirection: 'row',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'space-between'
  },
  headlines: {
    backgroundColor: '#B4E9FF',
    margin: 8,
    padding: 8
  },
  headlinesText: {
    color: '#003366',
    fontSize: 18,
    fontWeight: 'bold'
  }
  
});
