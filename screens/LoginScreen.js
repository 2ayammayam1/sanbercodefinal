import React, {Component} from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { CommonActions } from '@react-navigation/native';

export default class LoginScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

    loginHandler() {
      if(this.state.userName === 'marief' && this.state.password === 'marief'){
        this.state.isError = false;
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              { name: 'Home' },
            ],
          })
        );
      } else {
        this.state.isError = true;
      }
      
    }

    render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titleText}>Login</Text>
        {this.state.isError && <Text style={styles.errorText}>Email atau Password Salah!</Text>}
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Username/Email</Text>
          <TextInput style={styles.input} onChangeText={userName => this.setState({ userName })}/>
        </View>
        <View style={{padding: 15, marginHorizontal: 50}}>
          <Text>Password</Text>
          <TextInput style={styles.input} onChangeText={password => this.setState({ password })}/>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity 
            style={[styles.button, {backgroundColor: '#3EC6FF'}]}
            onPress={() => this.loginHandler()}
            >
            <Text style={styles.buttonText}>Masuk</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    flex: 1
  },
  titleText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 36,
    margin: 20,
    fontFamily: 'Roboto'
  },
  errorText: {
    textAlign: 'center',
    color: 'red'
  },  
  input: {
    borderRadius: 10,
    backgroundColor: '#C4C4C4',
    borderColor: '#003366',
    height: 48,
    fontSize: 16
  },
  inputTitle: {
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10
  }, 
  buttonText: {
    color: '#FFFFFF',
    fontSize: 24
  }
  
});
