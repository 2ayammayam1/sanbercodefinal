import React, {Component} from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity } from 'react-native';


export default class HomeScreen extends Component {

    render() {
    return (
      <View style={styles.container}>
        
      </View>
    );
    }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    flex: 1
  },
  titleText: {
    textAlign: 'center',
    color: '#003366',
    fontSize: 36,
    margin: 20,
    fontFamily: 'Roboto'
  },
  input: {
    borderRadius: 10,
    backgroundColor: '#C4C4C4',
    borderColor: '#003366',
    height: 48,
    fontSize: 16
  },
  inputTitle: {
    borderColor: '#003366'
  },
  button: {
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10
  }, 
  buttonText: {
    color: '#FFFFFF',
    fontSize: 24
  }
  
});
