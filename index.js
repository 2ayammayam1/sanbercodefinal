import React, { Component } from 'react'
import { createSwitchNavigator } from 'react-navigation';
import { NavigationContainer, CommonActions } from "@react-navigation/native";
import { createStackNavigator, Header } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { FontAwesome } from '@expo/vector-icons';
import LoginScreen from "./screens/LoginScreen";
import AboutScreen from "./screens/AboutScreen";
import HomeScreen from "./screens/HomeScreen";
import DetailScreen from "./screens/DetailScreen";


import { StyleSheet, View, TouchableOpacity, Image, Text, SafeAreaView, Button} from 'react-native';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const signOut = ({navigation}) => (
  navigation.dispatch(
    CommonActions.reset({
      index: 1,
      routes: [
        { name: 'Login' }
      ],
    })
  )
)

/**/

const customContent = ({navigation}) => (
  <View style={{flex:1, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center'}}>
      <SafeAreaView forceInset={{ horizontal: 'never' }}>
          <Text style={{marginVertical: 8}}>Logout?</Text>
          <Button title="Ya" onPress={ () => signOut({navigation}) }/>
      </SafeAreaView>
  </View>
)

const ImageHeader = props => (
  <View style={{ backgroundColor: '#eee' }}>
    <Image
      style={[StyleSheet.absoluteFill, {height: Header.height, width: Header.width}]}
      source={require('./images/headerBack.png')}
    />
    <Header {...props} style={{ backgroundColor: 'transparent' }}/>
  </View>
);

const headerOptions = ({ navigation }) => ({
  header: (props) => <ImageHeader {...props} />,
  title: 'E-Berita',
  headerTitleAlign: 'center',
  headerStyle: { 
    backgroundColor: 'transparent',
    elevation: 0, //for android
    shadowOpacity: 0, //for ios
    borderBottomWidth: 0, //for ios
  },
  headerLeft: () => (
    <TouchableOpacity
      onPress={() => navigation.openDrawer()}
    >
    <FontAwesome name="navicon" style={{marginStart:10}} size={24} color="black" />
    </TouchableOpacity>
  ),
})

const headerBackOptions = ({ navigation }) => ({
  header: (props) => <ImageHeader {...props} />,
  title: 'E-Berita',
  headerTitleAlign: 'center',
  headerStyle: { 
    backgroundColor: 'transparent',
    elevation: 0, //for android
    shadowOpacity: 0, //for ios
    borderBottomWidth: 0, //for ios
  },
  headerLeft: () => (
    <TouchableOpacity
      onPress={() => navigation.goBack()}
    >
    <FontAwesome name="arrow-left" style={{marginStart:10}} size={24} color="black" />
    </TouchableOpacity>
  ),
})

const HomeScreens = () => (
  <Stack.Navigator >
        <Stack.Screen 
          name='Home' 
          component={HomeScreen}
          options={headerOptions}
          />
          <Stack.Screen 
            name='Detail' 
            component={DetailScreen}
            options={headerBackOptions}
          />
      </Stack.Navigator>
)

const AboutScreens = () => (
  <Stack.Navigator>
        <Stack.Screen 
          name='About' 
          component={AboutScreen}
          options={headerOptions}
          />
      </Stack.Navigator>
)
const DrawerScreens = () => (
  <Drawer.Navigator>
    <Drawer.Screen 
        name='Home' 
        component={HomeScreens}
        />
    <Drawer.Screen 
      name='About' 
      component={AboutScreens}
      />
    <Drawer.Screen 
      name='Logout' 
      component={customContent}
      />
  </Drawer.Navigator>
)

export default () => {
  return (
    <NavigationContainer >
      <Stack.Navigator initialRouteName="Login" >
        <Stack.Screen name='Login' component={LoginScreen}  options={ {headerShown: false }} />
        <Stack.Screen 
          name='Home' 
          component={DrawerScreens}
          options={{headerShown: false}}
          />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

